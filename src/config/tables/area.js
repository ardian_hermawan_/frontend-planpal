export default ([
    { text: 'Name', value: 'name', sortable: true },
    { text: 'Description', value: 'description', sortable: true, width: "70%"},
    { text: 'Action', value: 'action', align: 'end' },
])
