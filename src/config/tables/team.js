export default ([
  {
    text: 'Name', value: 'name'
  },
  {
    text: 'Owner', value: 'author.name'
  },
  {
    text: 'Created At', value: 'created_at'
  },
  {
    text: 'Actions',
    value: 'actions',
    width: '8vw'
  }
])