export default ([
    { text: 'Name', value: 'name', sortable: true, width: '30%' },
    { text: 'Permissions', value: 'permissions', sortable: true, width: '50%' },
    { text: 'Action', value: 'action', align: 'end', width: '20%' },
])
