export default ([
    { text: 'Name', value: 'name', sortable: true },
    { text: 'Email', value: 'email', sortable: true },
    { text: 'Role', value: 'role', sortable: true },
    { text: 'Action', value: 'action', align: 'end' },
])
