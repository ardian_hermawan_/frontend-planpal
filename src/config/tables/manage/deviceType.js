export default ([
    { text: 'Type', value: 'type', sortable: true },
    { text: 'Description', value: 'description', sortable: true, width: "70%"},
    { text: 'Action', value: 'action', align: 'end' },
])
