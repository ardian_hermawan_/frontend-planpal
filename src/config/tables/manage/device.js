export default ([
    { text: 'Name', value: 'name', sortable: true },
    { text: 'Description', value: 'description', sortable: true, width: "20%"},
    { text: 'IP Address', value: 'ip_address', sortable: true },
    { text: 'Area', value: 'area.name', sortable: true },
    { text: 'Device Type', value: 'type.type', sortable: true },
    { text: 'Interval', value: 'interval', sortable: true },
    { text: 'Action', value: 'action', align: 'end' },
])
