export default ([
    {  text: 'Room', value: 'room'},
    {  text: 'Category', value: 'bidangKeahlian'},
    {  text: 'Examiner', value: 'penguji'},
    {  text: 'Date Time', value: 'date'},
    {  text: 'Action', value: 'action', align : 'end', width: "20%"},
])
