export default {
    access : "device_access",
    show : "device_show",
    create : "device_create",
    edit : "device_edit",
    delete : "device_delete",
}
