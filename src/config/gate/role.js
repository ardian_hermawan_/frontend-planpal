export default {
  access : "role_access",
  show : "role_show",
  create : "role_create",
  edit : "role_edit",
  delete : "role_delete",
}