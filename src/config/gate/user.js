export default {
  access : "user_access",
  show : "user_show",
  create : "user_create",
  edit : "user_edit",
  delete : "user_delete",
}