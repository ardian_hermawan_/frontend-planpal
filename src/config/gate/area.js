export default {
    access : "area_access",
    show : "area_show",
    create : "area_create",
    edit : "area_edit",
    delete : "area_delete",
}
