export default {
  data: [],
  current_page: 1,
  from: 1,
  last_page: 1,
  next_page_url: null,
  per_page: '20',
  prev_page_url: null,
  to: 1,
  total: 1
}
