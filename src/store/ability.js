import ability from '@/config/ability'
export default (store) => {
  ability.update(store.state.account.permissions)

  return store.subscribe((mutation) => {
    switch (mutation.type) {
      case 'account/setPermissions':
        ability.update([{subject: "all", action: mutation.payload}])
        break
      case 'auth/reset_state':
        ability.update([{ actions: 'read', subject: 'all' }])
        break
    }
  })
}