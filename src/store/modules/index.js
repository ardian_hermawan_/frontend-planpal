import csrf from './csrf'
import auth from './auth'
import account from "./account";
import setting from "./interfaces/dashboard";
import notification from "./interfaces/notification"
import schedule from "./schedule";
import student from './student';
import lecturer from './lecturer';
import room from "./room";

export default {
    csrf,
    notification,
    auth,
    account,
    setting,
    schedule,
    student,
    lecturer,
    room
}
