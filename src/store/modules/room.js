import moment from "moment";
import _ from 'lodash'

export default {
    namespaced: true,
    state: {
        items: [
            {
                id:1,
                name: "schedule 1",
                room: '1',
                date: moment().add(3, 'days'),
                bidangKeahlian : [
                    {id: 1, name : "Networking" },
                    {id: 2, name : 'IOT'},
                    {id: 3, name : 'Website'}
                ],
                penguji : [
                    { id: 1, name: 'Roni'},
                    { id: 2, name: 'hera'},
                    { id: 3, name: 'olla'},
                ],
                schedule : [
                    {
                        id : 1,
                        studentId : 1,
                        studentName: 'ardian',
                        projectTitle: 'Otomatisasi Kebun',
                        date: moment().add(3, 'days'),
                        bidangKeahlian : [
                            {id: 1, name : "Networking" },
                            {id: 2, name : 'IOT'},
                            {id: 3, name : 'Website'}
                        ] ,
                        pembimbing:[
                            {id: 1,  name : 'arief'},
                            {id: 1, name : 'ifhan'}
                        ],
                        moderator : { id: 1, name : 'yoshi'},
                        penguji : [
                            { id: 1, name: 'ardian'},
                            { id: 2, name: 'heramawn'},
                            { id: 3, name: 'farrel'},
                        ],
                    },
                    {
                        id : 2,
                        studentId : 2,
                        studentName: 'Rian',
                        projectTitle: 'Otomatisasi Kewan',
                        date: moment().add(1, 'days'),
                        bidangKeahlian : [
                            {id: 1, name : "Networking" },
                            {id: 2, name : 'IOT'},
                            {id: 3, name : 'Website'}
                        ] ,
                        pembimbing:[
                            {id: 1,  name : 'arief'},
                            {id: 1, name : 'ifhan'}
                        ],
                        moderator : { id: 1, name : 'yoshi'},
                        penguji : [
                            { id: 1, name: 'ardian'},
                            { id: 2, name: 'heramawn'},
                            { id: 3, name: 'farrel'},
                        ],
                    }
                ]
            },
            {
                id:2,
                name: "schedule-old 2",
                room: '2',
                date: moment().add(1, 'days'),
                bidangKeahlian : [
                    {id: 1, name : "Networking" },
                    {id: 2, name : 'IOT'},
                    {id: 3, name : 'Website'}
                ],
                penguji : [
                    { id: 1, name: 'ardian'},
                    { id: 2, name: 'heramawn'},
                    { id: 3, name: 'farrel'},
                ],
                schedule : [
                    {
                        id : 1,
                        studentId : 1,
                        studentName: 'ardian',
                        projectTitle: 'Otomatisasi hewan',
                        date: moment().add(1, 'days'),
                        bidangKeahlian : [
                            {id: 1, name : "Networking" },
                            {id: 2, name : 'IOT'},
                            {id: 3, name : 'Website'}
                        ] ,
                        pembimbing:[
                            {id: 1,  name : 'arief'},
                            {id: 1, name : 'ifhan'}
                        ],
                        moderator : { id: 1, name : 'yoshi'},
                        penguji : [
                            { id: 1, name: 'ardian'},
                            { id: 2, name: 'heramawn'},
                            { id: 3, name: 'farrel'},
                        ],
                    },
                    {
                        id : 2,
                        studentId : 2,
                        studentName: 'Oke',
                        projectTitle: 'Otomatisasi Robot',
                        date: moment().add(4, 'days'),
                        bidangKeahlian : [
                            {id: 1, name : "Networking" },
                            {id: 2, name : 'IOT'},
                            {id: 3, name : 'Website'}
                        ] ,
                        pembimbing:[
                            {id: 1,  name : 'arief'},
                            {id: 1, name : 'ifhan'}
                        ],
                        moderator : { id: 1, name : 'yoshi'},
                        penguji : [
                            { id: 1, name: 'ardian'},
                            { id: 2, name: 'heramawn'},
                            { id: 3, name: 'farrel'},
                        ],
                    }
                ]
            },
            {
                id:3,
                name: "schedule-old 3",
                room: '3',
                date: moment().add(10, 'days'),
                bidangKeahlian : [
                    {id: 1, name : "Networking" },
                    {id: 2, name : 'IOT'},
                    {id: 3, name : 'Website'}
                ],
                penguji : [
                    { id: 1, name: 'Dora'},
                    { id: 2, name: 'Boots'},
                    { id: 3, name: 'Cael'},
                ],
                schedule : [
                    {
                        id : 1,
                        studentId : 1,
                        roomId : 1,
                        room: '1',
                        studentName: 'Lucis',
                        projectTitle: 'Otomatisasi Sungai',
                        date: moment().add(10, 'days'),
                        bidangKeahlian : [
                            {id: 1, name : "Networking" },
                            {id: 2, name : 'IOT'},
                            {id: 3, name : 'Website'}
                        ] ,
                        pembimbing:[
                            {id: 1,  name : 'arief'},
                            {id: 1, name : 'ifhan'}
                        ],
                        moderator : { id: 1, name : 'yoshi'},
                        penguji : [
                            { id: 1, name: 'ardian'},
                            { id: 2, name: 'heramawn'},
                            { id: 3, name: 'farrel'},
                        ],
                    },
                    {
                        id : 2,
                        studentId : 2,
                        roomId : 1,
                        room: '1',
                        studentName: 'Oke',
                        projectTitle: 'Otomatisasi Kewan',
                        date: moment().add(3, 'days'),
                        bidangKeahlian : [
                            {id: 1, name : "Networking" },
                            {id: 2, name : 'IOT'},
                            {id: 3, name : 'Website'}
                        ] ,
                        dosenPembimbing1: 'arief',
                        dosenPembimbing2 : 'ifhan',
                        moderator : 'yoshi',
                        penguji1: 'ardian',
                        penguji2: 'dani',
                        penguji3 : 'farrel'
                    }
                ]
            }
        ],
        currentItem:  {
            id:1,
            name: "schedule-old 1",
            room: '1',
            bidangKeahlian : [
                {id: 1, name : "Networking" },
                {id: 2, name : 'IOT'},
                {id: 3, name : 'Website'}
            ],
            penguji : [
                { id: 1, name: 'Roni'},
                { id: 2, name: 'hera'},
                { id: 3, name: 'olla'},
            ],
            schedule : [
                {
                    id : 1,
                    studentId : 1,
                    studentName: 'ardian',
                    projectTitle: 'Otomatisasi Kebun',
                    date: moment().add(3, 'days'),
                    bidangKeahlian : [
                        {id: 1, name : "Networking" },
                        {id: 2, name : 'IOT'},
                        {id: 3, name : 'Website'}
                    ] ,
                    pembimbing:[
                        {id: 1,  name : 'arief'},
                        {id: 1, name : 'ifhan'}
                    ],
                    moderator : { id: 1, name : 'yoshi'},
                    penguji : [
                        { id: 1, name: 'ardian'},
                        { id: 2, name: 'heramawn'},
                        { id: 3, name: 'farrel'},
                    ],
                },
                {
                    id : 2,
                    studentId : 2,
                    studentName: 'Rian',
                    projectTitle: 'Otomatisasi Kewan',
                    date: moment().add(1, 'days'),
                    bidangKeahlian : [
                        {id: 1, name : "Networking" },
                        {id: 2, name : 'IOT'},
                        {id: 3, name : 'Website'}
                    ] ,
                    pembimbing:[
                        {id: 1,  name : 'arief'},
                        {id: 1, name : 'ifhan'}
                    ],
                    moderator : { id: 1, name : 'yoshi'},
                    penguji : [
                        { id: 1, name: 'ardian'},
                        { id: 2, name: 'heramawn'},
                        { id: 3, name: 'farrel'},
                    ],
                }
            ]
        },
        detailItem:   {
            id:1,
            name: "schedule-old 1",
            room: '1',
            date: moment().add(3, 'days'),
            bidangKeahlian : [
                {id: 1, name : "Networking" },
                {id: 2, name : 'IOT'},
                {id: 3, name : 'Website'}
            ],
            penguji : [
                { id: 1, name: 'Roni'},
                { id: 2, name: 'hera'},
                { id: 3, name: 'olla'},
            ],
            schedule : [
                {
                    id : 1,
                    studentId : 1,
                    studentName: 'ardian',
                    projectTitle: 'Otomatisasi Kebun',
                    date: moment().add(3, 'days'),
                    bidangKeahlian : [
                        {id: 1, name : "Networking" },
                        {id: 2, name : 'IOT'},
                        {id: 3, name : 'Website'}
                    ] ,
                    pembimbing:[
                        {id: 1,  name : 'arief'},
                        {id: 1, name : 'ifhan'}
                    ],
                    moderator : { id: 1, name : 'yoshi'},
                    penguji : [
                        { id: 1, name: 'ardian'},
                        { id: 2, name: 'heramawn'},
                        { id: 3, name: 'farrel'},
                    ],
                },
                {
                    id : 2,
                    studentId : 2,
                    studentName: 'Rian',
                    projectTitle: 'Otomatisasi Kewan',
                    date: moment().add(1, 'days'),
                    bidangKeahlian : [
                        {id: 1, name : "Networking" },
                        {id: 2, name : 'IOT'},
                        {id: 3, name : 'Website'}
                    ] ,
                    pembimbing:[
                        {id: 1,  name : 'arief'},
                        {id: 1, name : 'ifhan'}
                    ],
                    moderator : { id: 1, name : 'yoshi'},
                    penguji : [
                        { id: 1, name: 'ardian'},
                        { id: 2, name: 'heramawn'},
                        { id: 3, name: 'farrel'},
                    ],
                }
            ]
        },
        filter: {
            per_page: 5,
            page: null,
            total: null,
            sortBy: [],
            desc: [],
            search: null
        },
        loading: false,
    },
    getters: {
        filter(state) {
            state.filter.per_page = state.filter.per_page ? parseInt(state.filter.per_page) : 5
            state.filter.page = state.filter.page ? parseInt(state.filter.page) : 1
            state.filter.total = state.filter.total ? parseInt(state.filter.total) : null
            state.filter.sortBy = state.filter.sortBy ? state.filter.sortBy : []
            state.filter.desc = state.filter.desc ? state.filter.desc : []
            state.filter.search = state.filter.search ? state.filter.search : null
            return state.filter
        }
    },
    mutations: {
        setItems(state, items) {
            state.items = items
            items.filter ? state.filter = {
                ...state.filter,
                per_page: items.per_page,
                page: items.current_page,
                total: items.total
            } : null
        },
        setCurrentItem(state, payload){
            state.currentItem = payload
        },
        setDetailItem(state, payload){
            state.detailItem = payload
        },
        setLoading(state, payload) {
            state.loading = payload
        },
        setFilter (state, filter) {
            filter.per_page ? state.filter.per_page = filter.per_page : ''
            filter.page ? state.filter.page = filter.page : ''
            filter.sortBy ? state.filter = {...state.filter, ...filter}  : null
            filter.desc ? state.filter = {...state.filter, ...filter} : null
            filter.search ? state.filter = {...state.filter, ...filter} : null
        },
    },
    actions: {
        //Gawe API

        editedItem({commit, state}, {scheduleId}){
            commit('setLoading', true)
            const promise = state.items.filter(data => data.id === scheduleId)
            commit('setCurrentItem', promise)
            commit('setLoading', false)
        },

        getDetailItem({commit, state}, {roomId}) {
            commit('setLoading', true)
            const promise = state.items.find(data => data.id === roomId)
            commit('setDetailItem', promise)
            commit('setLoading', false)
        },

        deleted({commit, state}, {roomId}){
            commit('setLoading', true)
            const items = _.cloneDeep(state.items)
            const index = items.findIndex(data => data.id === roomId)
            items.splice(index,1)
            console.log(items)
            commit('setItems', items)
            commit('setLoading', false)
        }
    }
}
