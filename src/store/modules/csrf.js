import Vue from 'vue'
import {MUTATIONS as MUTATION, STATUS} from '../baseTypes'

const url = {
  CSRF: '/sanctum/csrf-cookie'
}

const state = {
  status: STATUS.IDLE
}

const mutations = {
  [MUTATION.CHANGE_STATUS](state, payload) {
    state.status = payload
  }
}

const actions = {
  async fetchCsrf({state, commit}) {
    if (state.status === STATUS.IDLE) {
      commit(MUTATION.CHANGE_STATUS, STATUS.FETCHING)

      try {
        const response = await Vue.axios.get(url.CSRF)
        commit(MUTATION.CHANGE_STATUS, STATUS.IDLE)

        return response
      } catch (e) {
        commit(MUTATION.CHANGE_STATUS, STATUS.IDLE)
        throw e
      }
    }
    throw new Error('Resource busy!')
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
