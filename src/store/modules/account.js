import Vue from 'vue'

export default {
  namespaced: true,
  state: {
    user: {
      name : "dashboard"
    },
    permissions: [],
    routesConfig: null,
    loading: false,
  },
  getters: {
    user: state => {
      if (!state.user) {
        try {
          const user = localStorage.getItem(process.env.VUE_APP_USER_KEY)
          state.user = JSON.parse(user)
        } catch (e) {
          console.error(e)
        }
      }
      return state.user
    },
    permissions: state => {
      if (!state.permissions) {
        try {
          const permissions = localStorage.getItem(process.env.VUE_APP_PERMISSIONS_KEY)
          state.permissions = JSON.parse(permissions)
          state.permissions = state.permissions ? state.permissions : []
        } catch (e) {
          console.error(e.message)
        }
      }
      return state.permissions
    },
    roles: state => {
      if (!state.roles) {
        try {
          const roles = localStorage.getItem(process.env.VUE_APP_ROLES_KEY)
          state.roles = JSON.parse(roles)
          state.roles = state.roles ? state.roles : []
        } catch (e) {
          console.error(e.message)
        }
      }
      return state.roles
    },
    routesConfig: state => {
      if (!state.routesConfig) {
        try {
          const routesConfig = localStorage.getItem(process.env.VUE_APP_ROUTES_KEY)
          state.routesConfig = JSON.parse(routesConfig)
          state.routesConfig = state.routesConfig ? state.routesConfig : {}
        } catch (e) {
          console.error(e.message)
        }
      }
      return state.routesConfig
    }
  },
  mutations: {
    setUser (state, user) {
      state.user = user
    },
    setPermissions(state, permissions) {
      state.permissions = permissions
    },
    setRoutesConfig(state, routesConfig) {
      state.routesConfig = routesConfig
    },
    setLoading(state, payload) {
      state.loading = payload
    }
  },
  actions: {
    updateProfile({ commit, dispatch }, data) {
      commit('setLoading', true)
      const promise =  Vue.axios.put(Vue.api('user.profile.update'), data)
      promise.then(res => {
        commit('setUser', res.data.data.user)
        dispatch('onNotificationSuccess', "Your profile updated", {root: true})
      })
        .catch(e => {
          dispatch('onNotificationError', e, {root: true})
          commit('notification/setError', e, {root: true})
        })
        .finally(() => commit('setLoading', false))
      return promise
    },
    changePassword({ commit, dispatch }, data) {
      commit('setLoading', true)
      const promise =  Vue.axios.post(Vue.api('user.profile.change.password'), data)
      promise.then(res => dispatch('onNotificationSuccess', res.data.message, {root: true}))
        .catch(e => {
          dispatch('onNotificationError', e, {root: true})
          commit('notification/setError', e, {root: true})
        })
        .finally(() => commit('setLoading', false))
      return promise
    },
    fetchRoutesConfig({ commit, dispatch }) {
      commit('setLoading', true)
      const promise =  Vue.axios.get('api/api-route-list')
      promise.then(res => commit('setRoutesConfig', res.data))
        .catch(e => dispatch('onNotificationError', e, {root: true}))
        .finally(() => commit('setLoading', false))
      return promise
    },
    fetchUserProfile({ commit, dispatch }) {
      commit('setLoading', true)
      const promise =  Vue.axios.get(Vue.api('user.profile'))
      promise.then(res => commit('setUser', res.data.data.user))
        .catch(e => dispatch('onNotificationError', e.message, {root: true}))
        .finally(() => commit('setLoading', false))
      return promise
    },
    fetchPermission({ commit, dispatch }) {
      commit('setLoading', true)
      const promise =  Vue.axios.get(Vue.api('abilities.index'))
      promise.then(res => commit('setPermissions', res.data.data.abilities))
        .catch(e => dispatch('onNotificationError', e, {root: true}))
        .finally(() => commit('setLoading', false))
    }
  }
}
