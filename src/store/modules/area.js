import Vue from 'vue';

export default {
    namespaced: true,
    state: {
        items: [],
        detailItem: null,
        filter: {
            per_page: 5,
            page: null,
            total: null
        },
        loading: null,
    },
    getters: {
        filter(state) {
            state.filter.per_page = state.filter.per_page ? parseInt(state.filter.per_page) : 5
            state.filter.page = state.filter.page ? parseInt(state.filter.page) : 1
            state.filter.total = state.filter.total ? parseInt(state.filter.total) : null
            return state.filter
        }
    },
    mutations: {
        setItems(state, items) {
            state.items = items.data
            state.filter = {
                per_page: items.per_page,
                page: items.current_page,
                total: items.total
            }
        },
        setDetailItem(state, payload){
            state.detailItem = payload
        },
        setLoading(state, payload) {
            state.loading = payload
        },
        setFilter (state, filter) {
            state.filter.per_page = filter.per_page
            state.page = filter.page
        },
    },
    actions: {
        getDetailItem({commit, dispatch}, areaId){
            commit('setLoading', true)
            const promise = Vue.axios.get(Vue.api('user.areas.show', {area : areaId}))
            promise.then(res => commit('setDetailItem', res.data.data))
                .catch(e => dispatch('onNotificationError', e, { root: true}))
                .finally(() => commit('setLoading', false))
            return promise
        },

        getItems({state, commit, dispatch}, { status = 'active'}){
            commit('setLoading', true)
            const promise = Vue.axios.get(Vue.api('user.areas.index'), {
                params: {
                    ...state.filter,
                    status: status
                }
            })
            promise.then(res =>  commit('setItems', res.data.data))
                .catch(e => dispatch('onNotificationError', e, { root: true}))
                .finally(() => commit('setLoading', false))
            return promise
        },

        create({commit, dispatch}, data){
            commit('setLoading', true)
            const promise = Vue.axios.post(Vue.api('user.areas.store'), data)
            promise
                .then(res => dispatch('onNotificationSuccess', res.data.message, { root: true}))
                .catch(e => {
                    commit('notification/setError', e, {root: true})
                    dispatch('onNotificationError', e, {root: true})
                })
                .finally(() => commit('setLoading', false))
            return promise
        },

        update ({commit, dispatch}, { areaId, data}) {
            commit('setLoading', true)
            const promise = Vue.axios.put(Vue.api('user.areas.update', { area: areaId }), data)
            promise.then(res => dispatch('onNotificationSuccess', res.data.message, {root: true}))
                .catch((e) => {
                    commit('notification/setError', e, {root: true})
                    dispatch('onNotificationError', e, {root: true})
                })
                .finally(() => commit('setLoading', false))
            return promise
        },

        delete ({commit, dispatch}, areaId) {
            commit('setLoading', true)
            const promise =  Vue.axios.delete(Vue.api('user.areas.destroy', { area: areaId}))
            promise.then(res => dispatch('onNotificationSuccess', res.data.message, {root: true}))
                .catch(e => dispatch('onNotificationError', e, { root: true}))
                .finally(() => commit('setLoading', false))
            return promise
        },
    }
}
