import Vue from "vue";

export default {
    namespaced: true,
    state: {
        items : [],
        detailItem: {},
        filter: {
            per_page: 5,
            page: null,
            total: null,
            sortBy: [],
            desc: [],
            search: null
        },
        loading: false,
    },
    getters: {
        filter(state) {
            state.filter.per_page = state.filter.per_page ? parseInt(state.filter.per_page) : 5
            state.filter.page = state.filter.page ? parseInt(state.filter.page) : 1
            state.filter.total = state.filter.total ? parseInt(state.filter.total) : null
            state.filter.sortBy = state.filter.sortBy ? state.filter.sortBy : []
            state.filter.desc = state.filter.desc ? state.filter.desc : []
            state.filter.search = state.filter.search ? state.filter.search : null
            return state.filter
        }
    },
    mutations: {
        setItems(state, items) {
            state.items = items
            state.filter = {
                ...state.filter,
                per_page: items.per_page,
                page: items.current_page,
                total: items.total
            }
        },
        setItemEdited(state, payload){
            state.itemEdited = payload
        },
        setDetailItem(state, payload){
            state.detailItem = payload
        },
        setLoading(state, payload) {
            state.loading = payload
        },
        setFilter (state, filter) {
            filter.per_page ? state.filter.per_page = filter.per_page : ''
            filter.page ? state.filter.page = filter.page : ''
            filter.sortBy ? state.filter = {...state.filter, ...filter}  : null
            filter.desc ? state.filter = {...state.filter, ...filter} : null
            filter.search ? state.filter = {...state.filter, ...filter} : null
        },
    },
    actions: {
        //Gawe API
        getItems({state, commit, dispatch}){
            commit('setLoading', true)
            const promise = Vue.axios.get(Vue.api('jadwal.index'), {
                params: {
                    ...state.filter,
                    status: status
                }
            })
            promise.then(res =>  commit('setItems', res.data.data.data))
                .catch(e => dispatch('onNotificationError', e, { root: true}))
                .finally(() => commit('setLoading', false))
            return promise
        },

        getDetailItem({commit, dispatch}, scheduleId) {
            const promise = Vue.axios.get(Vue.api('jadwal.show', {jadwal : scheduleId}))
            promise.then(res => commit('setDetailItem', res.data.data))
                .catch(e => dispatch('onNotificationError', e, { root: true}))
                .finally(() => commit('setLoading', false))
        },

        editedItem({commit, state}, scheduleId){
            commit('setLoading', true)
            const promise = state.items.filter(data => data.id === scheduleId)
            commit('setItemEdited', promise)
            commit('setLoading', false)
        }
    }
}
