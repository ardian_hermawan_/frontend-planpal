import Vue from "vue";

export default {
    namespaced: true,
    state: {
        // items: [
        //     {
        //         name: 'Imanuddin',
        //         nip: '100900507',
        //         category: 'IoT',
        //         //date: moment().add(1, 'days').format('L'),
        //     },
        //     {
        //         name: 'Putri',
        //         nip: '100900411',
        //         category: 'Database',
        //         //date: moment().add(3, 'days').format('L'),
        //     },
        //     {
        //         name: 'Faiz',
        //         nip: '100900501',
        //         category: 'IoT',
        //         //date: moment().add(4, 'days').format('L'),
        //     },
        //     {
        //         name: 'Daffa',
        //         nip: '100900515',
        //         category: 'Database',
        //         //date: moment().add(10, 'days').format('L'),
        //     },
        //     {
        //         name: 'Dani',
        //         nip: '100900520',
        //         category: 'RPL',
        //         //date: moment().add(8, 'days').format('L'),
        //     },
        // ],
        items : {},
        detailItem: {},
        filter: {
            per_page: 5,
            page: null,
            total: null,
            sortBy: [],
            desc: [],
            search: null
        },
        loading: false,
    },
    getters: {
        filter(state) {
            state.filter.per_page = state.filter.per_page ? parseInt(state.filter.per_page) : 5
            state.filter.page = state.filter.page ? parseInt(state.filter.page) : 1
            state.filter.total = state.filter.total ? parseInt(state.filter.total) : null
            state.filter.sortBy = state.filter.sortBy ? state.filter.sortBy : []
            state.filter.desc = state.filter.desc ? state.filter.desc : []
            state.filter.search = state.filter.search ? state.filter.search : null
            return state.filter
        }
    },
    mutations: {
        setItems(state, items) {
            state.items = items
            state.filter = {
                ...state.filter,
                per_page: items.per_page,
                page: items.current_page,
                total: items.total
            }
        },
        setDetailItem(state, payload){
            state.detailItem = payload
        },
        setLoading(state, payload) {
            state.loading = payload
        },
        setFilter (state, filter) {
            filter.per_page ? state.filter.per_page = filter.per_page : ''
            filter.page ? state.filter.page = filter.page : ''
            filter.sortBy ? state.filter = {...state.filter, ...filter}  : null
            filter.desc ? state.filter = {...state.filter, ...filter} : null
            filter.search ? state.filter = {...state.filter, ...filter} : null
        },
    },
    actions: {
        getDetailItem({commit, dispatch}, dosenId){
            commit('setLoading', true)
            const promise = Vue.axios.get(Vue.api('dosen.show', {dosen : dosenId}))
            promise.then(res => commit('setDetailItem', res.data.data))
                .catch(e => dispatch('onNotificationError', e, { root: true}))
                .finally(() => commit('setLoading', false))
            return promise
        },

        getItems({state, commit, dispatch}){
            commit('setLoading', true)
            const promise = Vue.axios.get(Vue.api('dosen.index'), {
                params: {
                    ...state.filter
                }
            })
            promise.then(res =>  commit('setItems', res.data.data))
                .catch(e => dispatch('onNotificationError', e, { root: true}))
                .finally(() => commit('setLoading', false))
            return promise
        },

        create({commit, dispatch}, data){
            commit('setLoading', true)
            const promise = Vue.axios.post(Vue.api('dosen.store'), data)
            promise
                .then(res => dispatch('onNotificationSuccess', res.data.message, { root: true}))
                .catch(e => {
                    commit('notification/setError', e, {root: true})
                    dispatch('onNotificationError', e, {root: true})
                })
                .finally(() => commit('setLoading', false))
            return promise
        },

        update ({commit, dispatch}, { dosenId, data}) {
            commit('setLoading', true)
            const promise = Vue.axios.put(Vue.api('dosen.update', { dosen: dosenId }), data)
            promise.then(res => dispatch('onNotificationSuccess', res.data.message, {root: true}))
                .catch((e) => {
                    commit('notification/setError', e, {root: true})
                    dispatch('onNotificationError', e, {root: true})
                })
                .finally(() => commit('setLoading', false))
            return promise
        },

        delete ({commit, dispatch}, dosenId) {
            commit('setLoading', true)
            const promise =  Vue.axios.delete(Vue.api('dosen.destroy', { dosen: dosenId}))
            promise.then(res => dispatch('onNotificationSuccess', res.data.message, {root: true}))
                .catch(e => dispatch('onNotificationError', e, { root: true}))
                .finally(() => commit('setLoading', false))
            return promise
        },
    }
}
