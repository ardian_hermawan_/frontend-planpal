import routes from './routes/index'

const options = {
  mode: 'history',
  base:  process.env.NODE_ENV === 'production'
      ? '/project/planpal/'
      : '/',
  routes: [
    {
      path: '/auth',
      name: 'auth',
      meta: {
        auth: false
      },
      component: () => import("@/layouts/Authentication"),
      children: [
        {
          path: '/login',
          name: 'login',
          meta: {
            auth: false
          },
          component: () => import('@/pages/auth/Login')
        },
        {
          path: '/register',
          name: 'register',
          meta: {
            auth: false
          },
          component: () => import('@/pages/auth/Register')
        },
      ]
    },
    {
      path: 'area',
      meta: {title: "Device Type Management"},
      name: 'area',
      component: () => import('@/pages/area/AreaManagement'),
    },
    {
      path: '/email/verification/:userId/:token',
      name: 'email-verification',
      meta: {
        auth: false
      },
      component: () => import('@/pages/auth/EmailVerification'),
    },
    {
      path: '/reset-password',
      name: "reset-password",
      meta: {
        auth: false
      },
      component: () => import('@/pages/auth/ResetPassword'),
    },
    {
      path: '/forgot-password',
      name: 'forgot-password',
      meta: {
        auth: false
      },
      component: () => import('@/pages/auth/ForgotPassword')
    },
    {
      path: '/logout',
      name: 'logout',
      meta: {
        auth: true,
      },
      component: {
        created() {
          this.$store.dispatch('auth/logout')
              .finally(() => this.$router.push({ name: 'login' }))
        },
        render: () => {}
      }
    },
    {
      path: '403',
      name: '403',
      meta: {
        auth: false
      },
      component: () => import('@/pages/exception/403')
    },
    {
      path: '404',
      name: '404',
      meta: {
        auth: false
      },
      component: () => import('@/pages/exception/404')
    },
    {
      path: '500',
      name: '500',
      meta: {
        auth: false
      },
      component: () => import('@/pages/exception/404')
    },
    {
      path: '/',
      component: () => import('@/layouts/AdminLayout'),
      meta: { auth: false },
      redirect: {name: 'dashboard'},
      children: [
        {
          path: 'dashboard',
          name: 'dashboard',
          meta: {
            icon: 'dashboard',
          },
          component: () => import('@/views/dashboard/Dashboard'),
        },
        ...routes
      ],
    },
  ],
}

export default options
