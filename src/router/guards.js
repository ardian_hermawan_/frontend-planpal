import NProgress from 'nprogress/nprogress'
import store from '@/store'

const { getters } = store
const defaultRedirect = {name: 'dashboard'}

NProgress.configure({ showSpinner: true })

/**
 * @param to
 * @param from
 * @param next
 */
const progressStart = (to, from, next) => {
  // start progress bar
  if (!NProgress.isStarted()) {
    NProgress.start()
  }
  next()
}

const checkRouteExist = (to, from, next) => {
  if (!to.matched.length) {
    next({name: '404'})
  }
  next()
}

/**
 * @param to
 * @param from
 * @param next
 */
const loginGuard = (to, from, next) => {
  if (to.matched.some(record => record.meta.auth)) {
    if (getters['auth/authenticated']) {
      next()
    } else {
      next()
    }
  }
  next()
}

const redirectIfAuthenticated = (to, from, next) => {
  let preventPage = ['login', 'register', 'forgot-password', 'reset-password'];
  if (getters['auth/authenticated']) {
    if (to.matched.some(record => preventPage.includes(record.name))) {
      next(defaultRedirect)
    }
    next()
  }
  next()
}
/*
const permissionGuard = (to, from, next) => {
  const abilities = getters['account/permissions']
  const haveGate = to.matched.some(route => {
    return Object.prototype.hasOwnProperty.call(route.meta, 'gate')
  })
  const canNavigate = to.matched.some(route => {
    return abilities.includes(route.meta.gate)
  })
  if (!canNavigate && haveGate && from.path !== '/dashboard' ) {
    return next(defaultRedirect)
  }
  next()
}
*/

const progressDone = () => {
  // finish progress bar
  NProgress.done()
}

export default {
  beforeEach: [progressStart, redirectIfAuthenticated, checkRouteExist, loginGuard],
  afterEach: [progressDone]
}
