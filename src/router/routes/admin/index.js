import BlankView from "@/layouts/BlankView";
import user from "@/router/routes/admin/user";
import tester from "@/router/routes/admin/tester";
import student from "@/router/routes/admin/student";
import lecturer from "@/router/routes/admin/lecturer";
import schedule from "./schedule";

export default {
  path: 'admin',
  name: 'admin',
  component: BlankView,
  children: [
    user,
    tester,
    student,
    lecturer,
    schedule
  ]}
