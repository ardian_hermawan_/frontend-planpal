export default ({
    path: 'student',
    name: 'student',
    meta: {title: "Info Mahasiswa", icon: "mdi-school"},
    component: () => import('@/pages/student/StudentManagement'),
})
