export default ({
    path: 'tester',
    name: 'tester',
    meta: {title: "Table Test"},
    component: () => import('@/views/dashboard/tables/RegularTables'),
})
