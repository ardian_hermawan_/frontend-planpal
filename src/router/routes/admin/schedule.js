export default ({
    path: 'schedule',
    name: 'schedule',
    meta: {title: "Schedule", icon : "mdi-calendar"},
    component: () => import('@/pages/schedule/ScheduleManagement'),
})
