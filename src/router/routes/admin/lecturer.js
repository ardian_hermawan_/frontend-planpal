export default ({
    path: 'lecturer',
    name: 'lecturer',
    meta: {title: "Lecturer Data"},
    component: () => import('@/pages/lecturer/LecturerManagement'),
})
