import BlankView from "@/layouts/BlankView";

export default ({
  path: 'user',
  name: 'manage.user',
  redirect: {name: 'user.list'},
  meta: {title: "User Management"},
  component: BlankView,
  children: [
    {
      path: 'list',
      name: 'user.list',
      meta: {title: "User Management"},
      component: () => import('@/views/dashboard/tables/RegularTables'),
    },
    /*{
      path: 'role',
      name: 'user.role',
      meta: {title: "Role Management", gate: 'user_access'},
      component: () => import('@/pages/manage/role/RoleManagement')
    },*/
    {
      path: 'device',
      name: 'user.device',
      meta: {title: "Daftar User"},
      component: () => import('@/views/dashboard/tables/RegularTables'),
    }
  ]
})
