import BlankView from "@/layouts/BlankView";

export default ({
    path: 'pages',
    name: 'pages',
    redirect: {name: 'dashboard'},
    component: BlankView,
    children: [
        {
            name: 'user',
            path: 'user',
            meta: {
                title: 'User Management',
                // requiresAuth: true
            },
        },
    ]
})
