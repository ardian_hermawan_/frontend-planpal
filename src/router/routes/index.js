import admin from './admin'
import user from './user'
//import tester from "@/router/routes/admin/tester"
import student from '@/router/routes/admin/student';

export default [
  admin,
  user,
  //tester,
  student
]
