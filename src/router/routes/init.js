export default ({
  path: '/init',
  children: [
    // Pages
    {
      name: 'User Profile',
      path: 'pages/user',
      meta: { title: 'User Profile', requiresAuth: true },
      component: () => import('@/views/dashboard/pages/UserProfile'),
    },
    {
      name: 'Notifications',
      path: 'components/notifications',
      component: () => import('@/views/dashboard/component/Notifications'),
    },
    {
      name: 'Icons',
      path: 'components/icons',
      component: () => import('@/views/dashboard/component/Icons'),
    },
    {
      name: 'Typography',
      path: 'components/typography',
      component: () => import('@/views/dashboard/component/Typography'),
    },
    // Tables
    {
      name: 'Regular Tables',
      path: 'tables/regular-tables',
      component: () => import('@/views/dashboard/tables/RegularTables'),
    },
    // Maps
    {
      name: 'Google Maps',
      path: 'maps/google-maps',
      component: () => import('@/views/dashboard/maps/GoogleMaps'),
    },
    // Upgrade
    {
      name: 'Upgrade',
      path: 'upgrade',
      component: () => import('@/views/dashboard/Upgrade'),
    },
  ],
})
