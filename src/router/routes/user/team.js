import BlankView from "@/layouts/BlankView";
import teamGate from "@/config/gate/team"

export default {
  path: 'team',
  name: 'team',
  component: BlankView,
  meta: {gate: teamGate.access},
  redirect: {name: 'team.list'},
  children: [
    {
      path: 'list',
      name: 'team.list',
      meta: {title: "Team Management"},
      component: () => import('@/pages/user/team/Teams')
    },
    {
      path: 'invitation',
      name: 'team.invitation',
      redirect: {name: 'team.invitation.page'},
      component: () => import('@/layouts/Fullscreen'),
      children: [
        {
          path: ':id',
          name: 'team.invitation.page',
          component: () => import('@/pages/user/team/TeamInvitation'),
        }
      ]
    }
  ]
}
