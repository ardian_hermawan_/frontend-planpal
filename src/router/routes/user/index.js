import BlankView from "@/layouts/BlankView";
import profile from "@/router/routes/user/profile";

export default {
  path: 'user',
  name: 'user',
  component: BlankView,
  children: [
    profile,
  ],
}
