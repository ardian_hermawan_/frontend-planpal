export default ({
  path: 'profile',
  name: 'user.profile',
  meta: {title: "Profile User", gate: "profile_show"},
  component: () => import("@/pages/user/profile/Profile"),
})