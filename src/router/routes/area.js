import areaGate from "@/config/gate/area";

export default {
    path: 'area',
    meta: { title: "Area Management", gate: areaGate.access},
    name: 'area',
    component: () => import('@/pages/area/AreaManagement'),
}
