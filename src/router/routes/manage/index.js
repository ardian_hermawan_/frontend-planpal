import BlankView from "@/layouts/BlankView";
import deviceType from "./deviceType";
import device from  "./device";

export default {
    path: 'manage',
    name: 'manage',
    component: BlankView,
    children: [
        deviceType,
        device
    ],
}
