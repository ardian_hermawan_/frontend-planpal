import deviceTypeGate from "@/config/gate/deviceType";

export default {
    path: 'devicetype',
    meta: {  gate: deviceTypeGate.access, title: "Device Type Management"},
    name: 'devicetype',
    component: () => import('@/pages/manage/deviceType/DeviceTypeManagement'),
}
