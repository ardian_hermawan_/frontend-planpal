

export default {
    path: 'device',
    name: 'device',
    meta: {   title: "Device Management"},
    component: () => import('@/pages/manage/device/DeviceManagement'),
}
